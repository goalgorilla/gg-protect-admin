<?php

# This module implements several hooks to make sure user 1 is not altered or blocked
# by users who shouldn't do that.
# It assumes that when something should actually be edited to user 1, it can be done
# via access to the database.
#
# Drupal 7 has default protection against cancellation/deletion of user 1. This module
# assumes that protection works. Protection against deletions via other modules are
# not in the scope of this module. It is not very interesting either, even a simple
# database query could delete the account, without this module being able to do
# anything against it.
#
# Variables that can be set:
# - protect_admin_custom_deny_destination
#     Set a valid URI if you want users to be redirected to the URI when protection is
#     activated. Defaults to admin/people.
# - protect_admin_deny_any_editing
#     Set to TRUE if you do not want other users to be able to change anything about
#     user 1. Defaults to FALSE.
#
# Created by Bram ten Hove, GoalGorilla.
# October 8th, 2012.
#
# TODO:
# - Think about implementing e-mail notifications to user 1.

/**
 * Implements hook_user_presave().
 *
 * The implementation of this hook prevents users other than user 1, from editing certain parts of user 1.
 * For now it only protects the:
 * - username
 * - e-mailaddress
 * - password
 * - status
 */
function gg_protect_admin_user_presave(&$edit, $account, $category) {
  global $user;

  // Get the path.
  $path = explode('?', request_uri());

  // Only act if Drush isn't involved. Also check for new site installation via
  // url.
  if (!drupal_is_cli() && $path[0] != '/install.php') {
    // Is someone, other than user 1, trying to change user 1? Act!
    if ($account->uid == 1 && $account->uid != $user->uid) {
      // Set a variable which we will check at the end to see if we need to protect the user.
      $protect = FALSE;

      // Deny changing the username.
      if ($edit['name'] != $account->name) {
        $protect = TRUE;
      }

      // Deny changes to the e-mailaddress.
      if ($edit['mail'] != $account->mail) {
        $protect = TRUE;
      }

      // Deny changes to the password.
      if (isset($edit['pass'])) {
        $protect = TRUE;
      }

      // Protect user 1 from being blocked.
      if ($edit['status'] == 0) {
        $protect = TRUE;
      }

      if ($protect === TRUE) {
        // Someone other than user 1 tries to cancel the account.
        drupal_set_message(t('You tried to edit the user 1 account, it is however protected. Please consult with user 1 about this matter. If you are sure this account should be edited, your best bet is through direct access to the database.'), 'warning');

        // Log this action.
        watchdog('protect_admin', t('%user attempted to edit the user 1 account.'), array('%user' => $user->name), WATCHDOG_WARNING);

        // Terminate the process by redirecting to a custom destination or admin/people.
        drupal_goto(variable_get('protect_admin_custom_deny_destination', 'admin/people'));
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * With this hook implementation we target the user profile form specifically.
 * We want to see if a variable is set that asks this module to prevent all editing to user 1
 * by users other than user 1.
 */
function gg_protect_admin_form_user_profile_form_alter(&$form, &$form_alter, $form_id) {
  global $user;

  // We only want to verify some things if the user 1 profile is being edited.
  if ($form['#user']->uid == 1) {
    // If the user is someone other than user 1, proceed with checking.
    if ($user->uid != 1) {
      // Check if any editing to the user 1 profile by other users should be prevented.
      // Else there is still some protection in hook_user_presave().
      if (variable_get('protect_admin_deny_any_editing', FALSE) === TRUE) {
        // Someone other than user 1 tries to cancel the account.
        drupal_set_message(t('You tried to edit the user 1 account, it is however protected. Please consult with user 1 about this matter. If you are sure this account should be edited, your best bet is through direct access to the database.'), 'warning');

        // Log this action.
        watchdog('protect_admin', t('%user attempted to edit the user 1 account.'), array('%user' => $user->name), WATCHDOG_WARNING);

        // Terminate the process by redirecting to a custom destination or admin/people.
        drupal_goto(variable_get('protect_admin_custom_deny_destination', 'admin/people'));
      }
    }
  }
}
